package ru.alexeyzhulin.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.SyncStateContract;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import static ru.alexeyzhulin.notifications.MainActivity.STARTFOREGROUND_ACTION;
import static ru.alexeyzhulin.notifications.MainActivity.STOPFOREGROUND_ACTION;

/**
 * Created by Alex on 20.03.2018.
 */

public class SimpleService extends Service {

    private static final String ANDROID_CHANNEL_ID = "hide";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (intent.getAction().equals(STARTFOREGROUND_ACTION)) {

                // Create the NotificationChannel
                CharSequence name = "Channel for Foreground service";
                String description = "Some description";
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel mChannel = new NotificationChannel(ANDROID_CHANNEL_ID, name, importance);
                mChannel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                NotificationManager notificationManager = (NotificationManager) getSystemService(
                        NOTIFICATION_SERVICE);
                notificationManager.createNotificationChannel(mChannel);


                Notification notification = new Notification.Builder(this, ANDROID_CHANNEL_ID)
                        .setContentTitle("Content title")
                        .setTicker("Ticket")
                        .setContentText("Content text")
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentIntent(pendingIntent)
                        .setOngoing(true)
                        .build();
                startForeground(777, notification);
            } else if (intent.getAction().equals(STOPFOREGROUND_ACTION)) {
                stopForeground(true);
                stopSelf();

                // Delete notification channel
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.deleteNotificationChannel(ANDROID_CHANNEL_ID);
            }

        } else {
            if (intent.getAction().equals(STARTFOREGROUND_ACTION)) {

                Notification notification = new Notification.Builder(this)
                        .setContentTitle("Content title")
                        .setTicker("Ticket")
                        .setContentText("Content text")
                        //.setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentIntent(pendingIntent)
                        .setOngoing(true)
                        .build();
                startForeground(777, notification);
            } else if (intent.getAction().equals(STOPFOREGROUND_ACTION)) {
                stopForeground(true);
                stopSelf();
            }
        }
        return Service.START_STICKY;
    }

}
