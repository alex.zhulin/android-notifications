package ru.alexeyzhulin.notifications;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.annotation.RequiresApi;
import android.util.Log;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class AppNotificationListenerService extends NotificationListenerService {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("NotificationListener", "Service started");

    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn){
        String key = sbn.getKey(); // Need minSdkVersion = 20
        Log.d("NotificationListener", "onNotificationPosted (" + key + ")");
        cancelNotification(key); // Need minSdkVersion = 21
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public void onNotificationRemoved(StatusBarNotification sbn){
        String key = sbn.getKey();
        Log.d("NotificationListener", "onNotificationRemoved (" + key + ")");
    }
}
